package com.taras;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ForMockTest {

	private static long startTime = System.currentTimeMillis();

	@InjectMocks
	ForMock forMock;

	@Mock
	MinCalc calc;

	@AfterAll
	static void tearDownAfterClass() throws Exception {

		long timeSpent = System.currentTimeMillis() - startTime;

		System.out.println("End  of Mock test. Time spent for test: " + timeSpent + " MS");
	}

	@Test
	void testSalary() {

		assertThrows(RuntimeException.class, () -> forMock.salary(23, 50));
	}

	@Test
	void testWeekendDays() {

		MinCalc calc = Mockito.mock(MinCalc.class);

		ForMock forMock = new ForMock(calc);

		when(calc.weekendDays(10, 15)).thenReturn(25);

		assertEquals(forMock.weekendDays(10, 15), 25);
	}

	@Test
	void testOverTimeBonus() {

		when(calc.overTimeBonus(10, 2)).thenReturn(20);

		assertEquals(20, forMock.overTimeBonus(10, 2));
	}

	@Test
	void testBonusForYearWorking() {

		MinCalc calc = Mockito.mock(MinCalc.class);

		ForMock forMock = new ForMock(calc);

		when(calc.bonusForYearWorking(1000, 10)).thenReturn(100);

		assertEquals(forMock.bonusForYearWorking(1000, 10), 100);
	}
}
