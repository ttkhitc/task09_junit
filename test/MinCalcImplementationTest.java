package com.taras;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MinCalcImplementationTest {

	private static long startTime = System.currentTimeMillis();
	private static MinCalcImplementation minCalc = new MinCalcImplementation();

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.out.println("Start");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		long timeSpent = System.currentTimeMillis() - startTime;
		System.out.println("End. Time of test: " + timeSpent + " MS" );
	}

	@BeforeEach
	void setUp() throws Exception {

		System.out.println("Next one");
	}

	@AfterEach
	void tearDown() throws Exception {
		System.out.println("End of one");
	}

	@Test
	void testSalary() {

		assertEquals(300, minCalc.salary(50, 6));

		assertEquals(200, minCalc.salary(50, 4));

		assertEquals(100, minCalc.salary(10, 10));

		assertNotEquals(500, minCalc.salary(50, 5));
	}

	@Test
	void testWeekendDays() {

		assertEquals(20, minCalc.weekendDays(3, 14));

		assertNotEquals(21, minCalc.weekendDays(3, 14));
	}

	@Test
	void testOverTimeBonus() {

		assertEquals(50, minCalc.overTimeBonus(5, 10));

		assertNotEquals(35, minCalc.overTimeBonus(2, 20));

		assertNotEquals(5, minCalc.overTimeBonus(0, 0));
	}

	@Test
	void testBonusForYearWorking() {
		assertEquals(0, minCalc.bonusForYearWorking(0, 10000));

		assertNotEquals(190, minCalc.bonusForYearWorking(1000, 4));

		assertEquals(200, minCalc.bonusForYearWorking(1000, 4));

		assertNotEquals(35, minCalc.bonusForYearWorking(2, 20));

	}

	@Test
	void testThisSalary() {
		
     minCalc.thisSalary(5, 10);
     
     assertEquals(50, minCalc.getSalary());
     
 	assertNotEquals(35,  minCalc.getSalary());
	}

}
