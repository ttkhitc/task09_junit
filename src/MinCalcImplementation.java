package com.taras;
public class MinCalcImplementation implements MinCalc {
// Using methods from MinCalc interface to create simple classes for testing
	
	private int salary;
	
	static final double MY_OWN_PI = 3.14d;
	
	
public int getSalary() {
	
		return salary;
	}

@Override
	public int salary(int rate, int time) {

		return rate * time;

	}
@Override
	public int weekendDays(int yearsWOrking, int atndsrtMinDays) {

		return atndsrtMinDays + yearsWOrking * 2;

	}
@Override
	public int overTimeBonus(int rate, int overTime) {

		return rate * overTime;

	}
@Override
	public int bonusForYearWorking(int rate, int yearsWorking) {

	   if(rate > 0) {
		return (rate / 20) * yearsWorking;
	   } else {
		   
		   return 0;
	   }
	}

public void thisSalary(int rate, int time) {
	
	int salary = rate*time;
	this.salary = salary;
}

}
