package com.taras;
public interface MinCalc {

	public int salary(int rate, int time);

	public int weekendDays(int yearsWOrking, int atndsrtMinDays);

	public int overTimeBonus(int rate, int overTime);

	public int bonusForYearWorking(int rate, int yearsWorking);

}
