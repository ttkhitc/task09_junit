package com.taras;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ForMock {
	// Using MinCalc interface instance to create class for Mock testing
	
	private MinCalc calc;

	public ForMock(MinCalc calc) {

		this.calc = calc;
	}

	public ForMock() {

	}

	public void setCalc(MinCalc calc) {
		this.calc = calc;
	}

	public int salary(int rate, int time) {

		throw new RuntimeException();
	}

	public int weekendDays(int yearsWOrking, int atndsrtMinDays) {

		return calc.weekendDays(yearsWOrking, atndsrtMinDays);
	}

	public int overTimeBonus(int rate, int overTime) {

		calc.overTimeBonus(rate,overTime);
		
		return rate*overTime;
	}

	public int bonusForYearWorking(int rate, int yearsWorking) {

		return calc.bonusForYearWorking(rate, yearsWorking);
	}

	public void readFile() throws IOException {

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter text");
		String result = "";
		String s;
		do {
			s = reader.readLine();
			result += s + " ";
		} while (s != null);
		
		System.out.println(result);
		
		reader.close();
	}

}
